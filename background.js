'use strict';
let re = new RegExp("https?://refraction.tech/test_ext/")
let firstAttempt;
console.log("work")
chrome.runtime.onInstalled.addListener(function (details) {
    if (details.reason == "install") {
        firstAttempt = true;
    } else if (details.reason == "update") {
        console.log("updated")
    }
});
chrome.runtime.onMessage.addListener(
    function (request, sender, sendResponse) {
        if (request.token) {
            sendResponse({farewell: "shown"});
        } else {
            sendResponse({farewell: "hidden"});
        }
    });


function handleUpdated(tabId, changeInfo, tabInfo) {
    console.log("handleUpdated")
    if (re.test(tabInfo.url)) {
        chrome.storage.sync.set({'flag': true}, function () {
        })
    } else chrome.storage.sync.set({'flag': false}, function () {
    })
}

chrome.tabs.onUpdated.addListener(handleUpdated);

function handleOnActivated(activeInfo) {
    console.log("handleOnActivated")
    chrome.tabs.query({
            active: true,
            currentWindow: true
        },
        function (tabs) {
            let tab = tabs[0];
            console.log(tab.incognito)

            if (re.test(tab.url)) {
                if (firstAttempt) {
                    chrome.tabs.executeScript(tab.id, {
                        file: "inject.js",
                        allFrames: true
                    });
                    firstAttempt = false;
                    chrome.storage.sync.set({
                        'flag': true
                    }, function () {
                    });
                    return;
                }
                chrome.storage.sync.set({
                    'flag': true
                }, function () {
                })
            } else chrome.storage.sync.set({'flag': false}, function () {
            })
        }
    )
}

chrome.tabs.onActivated.addListener(handleOnActivated);