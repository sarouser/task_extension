console.log("injected")
function injectScript(file_path, tag) {
    var node = document.getElementsByTagName(tag)[0];
    var script = document.createElement('script');
    script.setAttribute('type', 'text/javascript');
    node.appendChild(script);
    script.setAttribute('src', file_path);
}

injectScript(chrome.extension.getURL('content.js'), 'head');


window.addEventListener('message', recieveMessage, false);

function recieveMessage(event) {
    console.log("message recieved ")
    if (event.source !== window) {
        return;
    }
    let data = event.data;
    if (typeof data !== 'object' || data === null || !data.token) {
        return;
    }
    // var premiumExtensionId = "docejnldebmhedpaclnkellpfbebackb";

    //We don't need to hard code the extension ID, so by omitting it, the default is this extension's ID
    chrome.runtime.sendMessage( {token: data}, function (response) {

        if (response.farewell == "shown") {
            chrome.storage.sync.set({"token": data.token}, function () {
            });
        }
    });
}