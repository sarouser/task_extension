function test(obj, domOBJ) {
    chrome.storage.sync.get(['flag'], function (result) {
        if (result.flag) {
            chrome.storage.sync.get(['token'], function (result) {
                obj.flag = Boolean(result.token);
                obj.token = result.token;
                if (obj.flag) {
                    domOBJ.innerText = `Here is your Token \n ${obj.token}`;
                    domOBJ.style.display = "block";
                    document.getElementsByClassName("button")[0].style.display = "none";
                } else {
                    domOBJ.style.display = "none";
                }
            });
        }
    });

}

document.addEventListener("DOMContentLoaded", function () {
    let testObject = {
        flag: false,
        token: ""
    };

    let element = document.getElementsByClassName("tokenInfo")[0];
    test(testObject, element);

    chrome.tabs.onActivated.addListener(function (activeInfo) {
        chrome.tabs.get(activeInfo.tabId, function (tab) {
            if (re.test(tab.url)) {
            }
        });
    });
})

let re = new RegExp("https?://localhost/infoPage")

chrome.tabs.onActivated.addListener(function (activeInfo) {
    chrome.tabs.get(activeInfo.tabId, function (tab) {
        if (re.test(tab.url)) {
            console.log("right URL")
        }
        console.log(tab.url);
    });
});